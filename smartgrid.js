var smartgrid = require('smart-grid');

var settings = {
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '120px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    oldSizeStyle: false,
    container: {
        maxWidth: '1920px', /* max-width оn very large screen */
        fields: '300px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1500px',
            fields: '30px',
            offset: '30px'
        },
        md: {
            width: '960px',
            fields: '20px'
        },
        sm: {
            width: '768px',
            fields: '10px'
        }
    }
};

smartgrid('./src/scss', settings);