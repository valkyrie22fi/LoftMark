jQuery(document).ready(function() {
    jQuery('.nav').click(function() {
        jQuery('.header_center').toggle();
    });
    jQuery('.auth').click(function() {
        jQuery('.header_right').toggle();
    });
    jQuery('.news_title').click(function() {
        jQuery(this).next('.news_content').toggle();
    });
});